
"""
USAGE: 
   o install in develop mode: navigate to the folder containing this file,
                              and type 'python setup.py develop --user'.
                              (ommit '--user' if you want to install for 
                               all users)                           
"""
from setuptools import setup

setup(name='ik_draw',
      version='1.0',
      description='Draw some nice geometrical sketches, for those like me who can\'t draw with inkscape etc..',
      url='',
      author='Ilyas Kuhlemann',
      author_email='ilyasp.ku@gmail.com',
      license='MIT',
      packages=['ik_draw'],
      entry_points={
          "console_scripts": [],
          "gui_scripts": []
      },
      install_requires=['numpy',
                        'matplotlib'],
      zip_safe=False)
