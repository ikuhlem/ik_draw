import matplotlib.pyplot as plt
import numpy as np
from ik_draw.draw import circle, tube, get_angle
from ik_draw.tools import prepare_ax


if __name__ == "__main__":
    plt.style.use('presentation')

    f, ax = plt.subplots(1, 1)
