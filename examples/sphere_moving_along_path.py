import matplotlib.pyplot as plt
import numpy as np
from ik_draw.draw import circle, tube, get_angle
from ik_draw.tools import prepare_ax


def _factor_shortest_distance_to_path(rel_path: np.ndarray, rel_pos: np.ndarray) -> float:
    """
    For a fixed bead in the path of the moving bead, compute factor s on path where the distance
    to the fixed bead is shortest. Path and position of the fixed beads have to be relative
    to the initial position of the moving bead.
    If s is not in [0, 1+relative_diameter], there will be no collision with the bead.
    """
    d = rel_path
    p = rel_pos
    return (d*p).sum()/(d**2).sum()
    
    
def plot_sphere_moving_along_path(ax, p0, p1, d, path):
    # moving sphere initial position
    circle(ax, p0, d/2)
    ax.plot(*p0, 'o', color='black')

    # tube
    tube(ax, p0, path, d/2, plot_kwargs={'linestyle': '--'})
    ax.quiver(*p0, *path, color='black', scale_units='xy', angles='xy', scale=1,
              width=0.006)

    # Sphere it collides with on its path
    circle(ax, p1, d/2)
    ax.plot(*p1, 'o', color='black')

def plot_factor_shortest_distance(ax, p0, p1, path, p_s_dist):    
    ax.plot(p_s_dist[0], p_s_dist[1], 'o', color='darkred')
    # ax.quiver(*p1, *(p_s_dist-p1), color='darkred', scale_units='xy', angles='xy', scale=1,
    #           width=0.006)
    ax.quiver(*p_s_dist, *(p1-p_s_dist), color='darkred', scale_units='xy', angles='xy', scale=1,
              width=0.006)

def _compute_factor_for_2r_intersection(s_shortest_distance, length_of_path,
                                        distance_to_path, radius):
    k = np.sqrt(4 * radius ** 2 - distance_to_path ** 2)
    return s_shortest_distance - k / length_of_path

def plot_2r_circle_cutting_line(ax, p0, p1, d, path, s_dist, p_s_dist, s_collision, p_collision):    
    v_p1_p0_dash = p_collision-p1
    phi_p_s_dist = get_angle(np.array([0., 0.]), p_s_dist-p1)
    phi_v_p1_p0_dash = get_angle(np.array([0., 0.]), v_p1_p0_dash)
    print(phi_p_s_dist, phi_v_p1_p0_dash)
    if phi_v_p1_p0_dash >= phi_p_s_dist:        
        gamma = phi_v_p1_p0_dash - phi_p_s_dist
        angle_range = [phi_p_s_dist-gamma, phi_v_p1_p0_dash]
    else:
        raise NotImplementedError
    circle(ax, p1, d, angle_range, plot_kwargs={'color': 'blue'})    
    circle(ax, p1, d, [angle_range[1], 2*np.pi + angle_range[0]],
           plot_kwargs={'linestyle': '--', 'color': 'blue'})
    lw = plt.rcParams['lines.linewidth']
    ax.plot([p_s_dist[0], p_collision[0]], [p_s_dist[1], p_collision[1]],
            color='darkred', lw=1.5*lw)
    ax.plot([p1[0], p_s_dist[0]], [p1[1], p_s_dist[1]], color='darkred', lw=1.5*lw)
    ax.plot([p1[0], p_collision[0]], [p1[1], p_collision[1]], color='darkred', lw=1.5*lw)

def plot_final_position(ax, p0, p1, d, path, p_collision):
    # moving sphere initial position
    circle(ax, p_collision, d/2)
    ax.plot(*p_collision, 'o', color='black')

    # tube
    tube(ax, p0, path, d/2, plot_kwargs={'linestyle': '--'})
    ax.quiver(*p0, *path, color='black', scale_units='xy', angles='xy', scale=1,
              width=0.006)

    # Sphere it collides with on its path
    circle(ax, p1, d/2)
    ax.plot(*p1, 'o', color='black')
        
def main_initial(p0, p1, d, path):
    f, ax = plt.subplots(1, 1)
    prepare_ax(ax)
    
    plot_sphere_moving_along_path(ax, p0, p1, d, path)

    ax.text(p0[0]-d/4, p0[1], r'$\mathbf{p_0}$', fontsize=18)
    ax.text(p1[0]+d/8, p1[1]-d/8, r'$\mathbf{p_1}$', fontsize=18)
    
    return f, ax

def main_shortest_dist(p0, p1, path, p_s_dist):
    f, ax = plt.subplots(1, 1)
    prepare_ax(ax)
    
    plot_sphere_moving_along_path(ax, p0, p1, d, path)
    plot_factor_shortest_distance(ax, p0, p1, path, p_s_dist)
    return f, ax

def main_plot_circle_intersect(p0, p1, d, path, s_dist, p_s_dist, s_collision, p_collision):

    f, ax = plt.subplots(1, 1)
    prepare_ax(ax)
    
    plot_sphere_moving_along_path(ax, p0, p1, d, path)
    plot_2r_circle_cutting_line(ax, p0, p1, d, path, s_dist, p_s_dist, s_collision, p_collision)

    lbl_d = p1 + 0.5*(p_s_dist - p1) + d * np.array([0.1, 0.0])
    ax.text(*lbl_d, r'$\mathbf{d}$', fontsize=18, color='darkred')
    lbl_2r = p1 + 0.7*(p_collision - p1) + d * np.array([-0.125, -0.2])
    ax.text(*lbl_2r, r'$\mathbf{2r}$', fontsize=18, color='darkred')
    lbl_k = 0.5 * (p_collision + p_s_dist) + d * np.array([0.0, 0.1])
    ax.text(*lbl_k, r'$\mathbf{k}$', fontsize=18, color='darkred')
    return f, ax

def main_plot_final(p_0, p_1, d, path, p_collision):
    f, ax = plt.subplots(1, 1)
    prepare_ax(ax)

    plot_final_position(ax, p_0, p_1, d, path, p_collision)
    return f, ax

if __name__ == "__main__":

    plt.style.use('presentation')
    
    # initial configuration
    p0 = np.array([0, 0])
    d = 1.0
    path = np.array([3, 0.6])
    p1 = np.array([3.0*3/4.2, -0.2*3/4])

    # point of shortest distance
    s_dist = _factor_shortest_distance_to_path(path, p1-p0)
    p_s_dist = p0 + s_dist * path

    # point of collision
    s_collision = _compute_factor_for_2r_intersection(s_dist, np.sqrt((path**2).sum()),
                                                      np.sqrt(((p_s_dist-p1)**2).sum()), d/2)
    p_collision = p0+s_collision*path

    fig1, ax1 = main_initial(p0, p1, d, path)


    fig2, ax2 = main_shortest_dist(p0, p1, path, p_s_dist)


    fig3, ax3 = main_plot_circle_intersect(p0, p1, d, path, s_dist, p_s_dist, s_collision, p_collision)

    fig4, ax4 = main_plot_final(p0, p1, d, path, p_collision)


    if False:
        fig1.savefig('sphere_collision_01.svg')
        fig2.savefig('sphere_collision_02.svg')
        fig3.savefig('sphere_collision_03.svg')
        fig4.savefig('sphere_collision_04.svg')        
    else:
        axes = [ax1, ax2, ax3, ax4]
        min_x, max_x = ax1.get_xlim()
        min_y, max_y = ax1.get_ylim()
        for ax in axes[1:]:
            xlim = ax.get_xlim()
            ylim = ax.get_ylim()
            if xlim[0] < min_x:
                min_x = xlim[0]
            if xlim[1] > max_x:
                max_x = xlim[1]
            if ylim[0] < min_y:
                min_y = ylim[0]
            if ylim[1] > max_y:
                max_y = ylim[1]

        xlim = [min_x, max_x]
        ylim = [min_y, max_y]
        for ax in axes:
            ax.set_xlim(*xlim)
            ax.set_ylim(*ylim)

        fig1.savefig('sphere_collision_01_aligned.svg')
        fig2.savefig('sphere_collision_02_aligned.svg')
        fig3.savefig('sphere_collision_03_aligned.svg')
        fig4.savefig('sphere_collision_04_aligned.svg')
    plt.show()
