from matplotlib.pyplot import Axes

def prepare_ax(ax: Axes):
    ax.set_aspect('equal')
    ax.set_axis_off()
