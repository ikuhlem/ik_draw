from typing import List
import numpy as np
from matplotlib.pyplot import Axes


def circle(ax: Axes, position, radius: float,
           angle_range: List[float]=[0, 2*np.pi], ndots: int=200,
           plot_kwargs={}):
    kwargs = {'linestyle': '-', 'color': 'black'}
    kwargs.update(plot_kwargs)
    position = np.array(position)
    theta = np.linspace(angle_range[0], angle_range[1], ndots)
    ax.plot(radius*np.cos(theta) + position[0],
            radius*np.sin(theta) + position[1], **kwargs)            

def tube(ax: Axes, start_pos, end_pos, radius: float,
         ndots: int=200, plot_kwargs={}):
    kwargs = {'linestyle': '-', 'color': 'black'}
    kwargs.update(plot_kwargs)
    start_pos = np.array(start_pos)
    end_pos = np.array(end_pos)

    phi = get_angle(start_pos, end_pos)
    
    start_a = start_pos + radius*np.array([np.cos(phi+np.pi/2), np.sin(phi+np.pi/2)])
    end_a = end_pos + radius*np.array([np.cos(phi+np.pi/2), np.sin(phi+np.pi/2)])
    start_b = start_pos + radius*np.array([np.cos(phi-np.pi/2), np.sin(phi-np.pi/2)])
    end_b = end_pos + radius*np.array([np.cos(phi-np.pi/2), np.sin(phi-np.pi/2)])

    x_a = np.linspace(start_a[0], end_a[0], ndots)
    x_b = np.linspace(start_b[0], end_b[0], ndots)

    ax.plot(x_a, (end_a[1] - start_a[1])/(end_a[0] - start_a[0]) * (x_a - x_a[0]) + start_a[1],
            **kwargs)
    ax.plot(x_b, (end_b[1] - start_b[1])/(end_b[0] - start_b[0]) * (x_b - x_b[0]) + start_b[1],
            **kwargs)


def get_angle(start_pos: np.ndarray, end_pos: np.ndarray) -> float:    
    v = end_pos - start_pos
    v = v / np.sqrt((v**2).sum())
    
    phi = np.arccos(v[0])
    

    if v[1] >= 0:
        return phi
    else:
        return 2*np.pi - phi
